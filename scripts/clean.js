// package.json 中去掉  "type": "module", 可以直接使用require，__dirname，但clean之后，又再次undefine,
// 建议保留"type": "module", 使用import，且自己定义__dirname

// const fs = require('fs-extra');
// const path = require('path');
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(fileURLToPath(import.meta.url));

// 要清理的文件和文件夹列表
const filesToDelete = [
    'node_modules',
    'dev-dist',
    'dist',
    'yarn.lock',
    'package-lock.json',
    // '.vscode',
    // '.idea',
];

// 清理函数
function cleanProject() {
    filesToDelete.forEach((file) => {
        const filePath = path.join(__dirname+"/../", file);
        fs.removeSync(filePath);
    });
}

// 执行清理
cleanProject();


// console.log(import.meta.url);
// console.log(__filename);
// console.log(__dirname);

// 输出如下：
// file:///Users/user/Desktop/demo/src/demo.js
// /Users/user/Desktop/demo/src/demo.js
// /Users/user/Desktop/demo/src
