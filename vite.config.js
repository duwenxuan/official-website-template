import {fileURLToPath, URL} from 'node:url'

import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import {resolve} from 'path';
import {VantResolver} from "@vant/auto-import-resolver";
import Components from 'unplugin-vue-components/vite';
import {ElementPlusResolver} from "unplugin-vue-components/resolvers";
import AutoImport from 'unplugin-auto-import/vite'
import {VitePWA} from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
    // publicDir:'./public',

    // 配置打包后html引用资源的基路径，若未在router中配置地址栏基路径也会影响地址栏基路径
    // 默认值为/，此时打包后代码必须放在网站根路径才能正常访问资源
    // 对应vue.config.js中的publicPath:'./'
    base: './',
    // 由于影响html引用资源的基路径，且会与router中的配置冲突，
    // 固一般只会取/或./，不用于配置地址栏基路径
    // base:"/base2",

    server: {
        // host: '0.0.0.0',//自定义主机名
        port: 9000,
        // https: false,
        proxy: {
            /*  '/api': {
                target: "http://gmall-h5-api.atguigu.cn", //跨域地址
                changeOrigin: true, //支持跨域
                  // rewrite: path => path.replace("/^\/path/", "")//将路径中的 /path/ 替换为 ""
              },*/
            '/*': {
                target: "http://localhost:9000", //跨域地址
                changeOrigin: true, //支持跨域
            }
        }
    },
    build: {
        rollupOptions: {
            /*放在public目录，,不需要进行该处理，打包就会按原目录结构直接放在dist/下，只是此时在开发环境url需要加上public访问
            * 放在public目录下还添加了resolve，还会生成dist/public/*，导致重复
            * */
            input: {
                main: resolve(__dirname, './index.html'),
                p1: resolve(__dirname, './pages-demo/sub/index.html'),
            },
        },
    },
    plugins: [
        vue({
            template: {
                compilerOptions: {
                    // avoid to print warnings in console
                    isCustomElement: tag => tag.startsWith('ainow-')
                    // isCustomElement: tag => tag==='ainow-link-box'
                }
            },
            script: {
                defineModel: true
            }
        }),
        //vant,ElementPlus按需引入，自动导入
        AutoImport({
            resolvers: [ElementPlusResolver()],
        }),
        Components({
            resolvers: [VantResolver(), ElementPlusResolver()],
        }),
        VitePWA({
            // PWA 插件的配置，
            // 开启后，html的rel="icon"等才能生效，且优先级更高
            manifest: {
                name: 'Your App Name',
                description: 'Your App Description',
                icons: [
                    {
                        src: "/favicon.ico",
                        sizes: "192x192",
                        type: "image/png"
                    },
                    // 你可以根据需要添加更多尺寸和类型的图标
                ],
            },
            workbox: {
                // 对所有匹配的静态资源进行缓存
                // globPatterns: ["**/*.{js,css,html,ico,png,svg}"],
            },
            devOptions: {
                // enabled: true
            },
            // 其他 PWA 插件配置...
        }),
    ],
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },
})
