import {createRouter, createWebHashHistory, createWebHistory} from 'vue-router'
import {nextTick} from 'vue'
// import { createRouter, createWebHistory,createWebHashHistory} from 'vue-router'

import HomeView from '@/a-views/HomeView.vue'
import {use_app_state_store} from "@/stores/systemStatus.js";
import {storeToRefs} from "pinia";
import SpaceView from "@/a-views/SpaceView.vue";


// const mainHeaderView = () => import('@/a-views/MainHeaderView.vue')
// const mainHeaderView = () => import('@/a-views/LoginView.vue')

/*
* 注意
* name重复的路由只会注册靠后的
* path重复的路由，匹配靠前的
* */
const router = createRouter({

    // historyj模式上线后有bug,即点击跳转正常，再次刷新或直接访问会404
    // mode: 'history',//history路由，地址栏不带#号；hash路由，地址栏带#号，默认值

    // 配置地址栏基路径，会覆盖vite.config.js中base
    //  history: createWebHistory(import.meta.env.BASE_URL),// 为vite.config.js中base
    // history: createWebHistory('/'),
    // history: createWebHistory('/base'),
    // hash模式下，/#hash定位需要使用RouterLink,a标签不行，history模式都可以
    history: createWebHashHistory(),

    // linkActiveClass: 'linkActiveClass',
    // linkExactActiveClass: 'linkExactActiveClass',

    // 当路由跳转后滚动条所在的位置,默认为上一路由所在位置
    scrollBehavior (to, from, savedPosition) {
        if (to.path==='/'){
            let appState = use_app_state_store()
            setTimeout(() => {
                appState.$patch({headerState: {transform: 'translateY(0)'}})
            }, 800)
            const element = document.querySelector(to.hash+'-offset');
            if (element) {
                const offsetTop = element.getBoundingClientRect().top + window.scrollY - 100;
                window.scrollTo({
                    top: offsetTop,
                    behavior: "smooth"
                });
            }
        }else {
            return { left: 0, top: 0 }
        }
    },

    routes: [
        {
            path: '/beggar/',
            redirect: 'http://110.41.180.142:7000/donate/',
        },
        {
            path: '/space',
            name: 'space',
            component: () => import('@/a-views/SpaceView.vue'),
        },
        {
            path: '/search',
            name: 'search',
            component:() => import('@/a-views/SearchView.vue'),
            meta:{
                title: 'Q享-搜索!'
            }
        },

        // to属性与path匹配的RouterLink会加上类:router-link-active router-link-exact-active
        // 而且此时to属性与其父级路由path匹配的RouterLink会加上类router-link-active
        {
            path: '/mainHeaderView',
            // route level code-splitting
            // this generates a separate chunk (About.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import('@/a-views/MainHeaderView.vue'),
            // redirect:'/news',
            //如果父级路由设置component，访问子路由会先在app.vue的RouterView(路由出口)中显示该组件，然后再在该组件的RouterView显示子组件，若该组件没有RouterView，子组件应该显示的位置会空白
            //若未设置component，访问子路由就直接在app.vue的RouterView中显示子组件
            // component: () => import('@/components/views/DemoView.vue'),
            meta: {
                title: 'Q享-齐分享，同乐趣，共进步!', // 设置该路由在侧边栏和面包屑中展示的名字
                // icon: 'el-icon-s-help' // 设置该路由的图标，对应路径src/assets/icons/svg
                // hidden: true, // 如果设置为true，则不会在侧边栏出现
                // noCache: true // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
                // breadcrumb: false // 如果设置为false，则不会在breadcrumb面包屑中显示
                // affix: true // 如果设置为true，则标签将永远显示在左侧
                // activeMenu: '/demo/note' // 如果设置路径，侧边栏将突出显示您设置的路径
                // roles: ['admin','editor'] // 设置该路由进入的权限，支持多个权限叠加
            },
            children: [
                {
                    path: '/',
                    name: 'home',
                    component: HomeView,
                },
                {
                    path: '/login',
                    name: 'login',
                    component: () => import('@/a-views/LoginView.vue')
                },
                {
                    path: '/menu',
                    name: 'menu',
                    meta: {
                        title: 'Q享-菜单',
                        transition: 'fade'
                    },
                    component: () => import('@/a-views/MenuView.vue')
                },
                {
                    path: '/product',
                    name: 'product',
                },

                {
                    path: '/demo',
                    name: 'demo',

                    children: [
                        {
                            path: '/noteOptionsApi', name: '/noteOptionsApi',
                            component: () => import('@/a-views/note/NoteOptionsApi.vue')
                        },
                        {
                            path: '/notebook', name: '/notebook',
                            component: () => import('@/a-views/NoteBook/NoteBook.vue')
                        },
                        {
                            path: '/demoZJ', name: '/demoZJ',
                            component: () => import('@/a-views/note/DemoZJ.vue')
                        },
                        {
                            path: '/cart', name: '/cart',
                            component: () => import('@/a-views/note/DemoCart.vue')
                        },
                        {
                            path: '/vuex', name: '/vuex',
                            component: () => import('@/a-views/note/NoteVuex.vue')
                        },
                        {
                            path: '/vuex2', name: '/vuex2',
                            component: () => import('@/a-views/note/NoteVuex2.vue')
                        },
                        {
                            path: '/news', name: 'demoNews',
                            meta: {
                                title: 'news',
                            },
                            component: () => import('@/a-views/note/DemoNews.vue')
                        },
                        {
                            path: '/table', name: 'demoTable',
                            component: () => import('@/a-views/note/DemoTableView.vue')
                        },
                        // 动态路由传参,?为参数可选符，不加的话表示必须传参，否则匹配不到组件，如/demo/note，/demo/note/显示空白
                        {
                            path: '/note/:words?', name: 'note',
                            component: () => import('@/a-views/note/NoteCompositionApiSimple.vue')
                        },


                    ]
                },
            ]
        },


        {
            path: '/mobile',
            name: 'mobile',
            component: () => import('@/a-views/MobileView.vue'),
            children: []
        },

        {
            path: '/status',
            component: () => import('@/a-views/status/status.vue'),
            children: [
                {
                    path: '/404',
                    name: '/404',
                    component: () => import('@/a-views/status/404.vue')
                }
            ]

        },

        // vue2使用*，vue3使用/:pathMatch(.*)或/:pathMatch(.*)*或/:catchAll(.*)
        {
            path: "/:pathMatch(.*)",
            component: () => import('@/a-views/status/404.vue')
            // 重定向地址栏source path会变化
            // redirect: "/404"
        },

    ],

})

let authPaths = ['/space', '/pay']
// let notDoNext = ['product', 'demo']
let notDoNext = ['product', 'demo','search','login']
let isAuthenticated = false
const titles = {
    menu: '菜单'
}

let headerInitialStateIn={
    home:{
        position:'fixed',
        showBehavior:'autoHide,transparentMode',
        bgc:'transparent',
        color: '#dfdfdf',
    },
    menu:{
        title:'菜单',
        position:'fixed',
        showBehavior:'titleMode',
        bgc: '#eee',
        color:'#101010',
    },
    common:{
        position:'fixed',
        showBehavior:'',
        bgc: 'white',
        color:'#101010',
    },

}

//导航守卫
router.beforeEach((to, from, next) => {
    // 肯定不放行
    if (notDoNext.includes(to.name)) {
        // next({name:from.name})
        next(false)
        return
    }
    // 放行
    if (authPaths.includes(to.path) && !isAuthenticated) {
        next({name: 'login'})
    } else {
        next()
    }
    // 修改状态
    // 注意：子路由未设置title,则默认为父路由的
    document.title = to.meta.title || 'Q享-welcome3'

    let appState = use_app_state_store()
    let headerInitialState = headerInitialStateIn[to.name] || headerInitialStateIn.common
    // console.log('headerInitialState',headerInitialState)
    appState.$patch({headerState:headerInitialState})
})

export default router
