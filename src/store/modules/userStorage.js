
const key="qx-userInfo"
//array,obj的存储都应该序列化
//vuex
export const getUserInfo=()=>{
    const defaultUserInfo={
        userId:-1,
        token:""
    }
    let userInfo = localStorage.getItem(key)
    return userInfo?JSON.parse(userInfo):defaultUserInfo;
}

export function setUserInfo(userInfo){
    let oldUserInfo = getUserInfo()
    let newUserInfo = {...oldUserInfo,...userInfo}
    localStorage.setItem(key,JSON.stringify(newUserInfo))
}
export function removeUserInfo(){
    localStorage.removeItem(key)
}
