const state={
    s1:1,
    s2:2,
    s3:3,
}

const mutations={
    s1add1(state,payload) {
        state.s1 += payload;
    },
    s2add1(state,payload) {
        state.s2 += payload;
    },
    s3add1(state) {
        state.s3 += 1;
    },
}

const actions={
    demo(context){
        //通过context可拿到
        context.getters
        // context.

        let payload={}
        //访问其他模块的mutations
        context.commit('user/setUserInfo',payload,{root:true})
    },
    s1add1Async(context,payload){
        setTimeout(()=>{
            context.commit('s1add1',payload)
        },1000)
    },
    s2add1Async(context,payload){
        setTimeout(()=>{
            context.commit('s2add1',payload)
        },1000)
    },
    s3add1Async(context){
        setTimeout(()=>{
            context.commit('s3add1')
        },1000)
    }
}
const getters={
    sg1(state){
        return state.s1+100
    },
    sg2(state){
        return state.s2+100
    },
    sg3(state){
        return state.s3+100
    },
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters
}
