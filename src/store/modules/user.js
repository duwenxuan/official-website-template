import axios from "axios";
import {getUserInfo, setUserInfo} from "@/store/modules/userStorage.js";
export default {
    namespaced:true,
    //写成函数，保证数据独立性
    state(){
        return {
            userInfo:getUserInfo()
        }
    },
    mutations:{
        setUserInfo(state,payload){
            state.userInfo = payload
            setUserInfo(payload)
        },
    },
    actions:{

    },
    getters:{

    }
}
