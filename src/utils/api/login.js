import axiosShop from "@/utils/axiosShop.js";
import axiosMine from "@/utils/axiosMine.js";

export const getImgCaptcha = () => {
    // return axiosShop.get('/captcha/image')
    return axiosMine.get('/captcha/img/line')
}

// 传body
export const getEmailCaptcha = (captchaValue, email, md5) => {
    return axiosMine.post('/sendEmailCaptcha', {
        form: {
            captchaValue,
            email,
            md5,
        }
    })
}

export const loginByEmail = ( email, emailCaptcha ) => {
    return axiosMine.post('/loginByEmail', {
        form: {
            email,
            emailCaptcha,
        }
    })
}

export const loginByPhone = () => {
    return axiosShop.post('/passport/login', {
        form: {
            "smsCode": "246810",
            "mobile": "18917286702",
            "isParty": false,
            "partyData": {}
        }
    })
}



