import axios from "axios";
import {showToast} from "vant";
import store, {useMapState} from "@/store/index.js";

// 智慧商城-实战项目 接口文档：https://apifox.com/apidoc/shared-12ab6b18-adc2-444c-ad11-0e60f5693f66
// 分离一个对baseURL独立配置的axios模块
const instance = axios.create({
    baseURL: 'http://cba.itlike.com/public/index.php?s=/api/',
    // baseURL: 'https://some-domain.com/api/',
    timeout: 5000,
    // headers: {'X-Custom-Header': 'foobar'}
})

let authUrls=['/cart/add']
// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    console.log('config', config)
    if (authUrls.includes(config.url)) {
        let accessToken = store.state.user.userInfo.token
        if (accessToken && accessToken !== '') {
            config.headers['Access-Token'] = accessToken
        }
    }
    config.headers.platform = 'H5'
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
})

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么

    console.log('response', response)
    const data = response.data
    if (data.status === 200) {
        return data.data
    } else if (data.status >= 200 && data.status < 300) {
        return {
            ...data.data,
            message: data.message,
        }
    } else {
        // Promise 会中断await后的程序
        // 实现await之后就是成功状态，在这里统一处理非成功状态
        showToast(data.message)
        return Promise.reject(data.message)
    }
}, function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
})

export default instance;

