
import { defineComponent, defineEmits } from 'vue';

const component1 = defineComponent({
    name: 'FooterView',
    setup(){
        const emit = defineEmits(['sendMsg'])
        return {
            emit
        }
    }

});
export default component1
