
export const loading = {
    inserted(el, binding, vnode, prevVnode) {

    },
    setup(el, binding, vnode, prevVnode) {

    },
    beforeMount(el, binding, vnode, prevVnode) {
        binding.value? el.classList.add('loading'): el.classList.remove('loading')
    },
    mounted(el, binding, vnode, prevVnode) {
    },
    updated(el, binding, vnode, prevVnode) {
        binding.value? el.classList.add('loading'): el.classList.remove('loading')
    },
    // 其他钩子函数...
};


export const focus = {
    mounted(el, binding, vnode, prevVnode) {
        el.focus()
    },
};
