import '../../assets/base.less'
import '../../assets/main.less'

import {createApp} from 'vue'
// import {createApp, defineCustomElement} from 'vue'
import App from './App.vue'
import router from '../../router/index.js'
import {createPinia} from "pinia"
import {focus, loading} from "@/utils/global-directives.js";
// import {createRouter} from "vue-router";
import store from "@/store/index.js";
import {Toast} from "vant";
//按需引入的组件可以不用导入，导入反而可能导致样式重复出现问题
//但是使用showToast等辅助函数唤起组件时，不导入样式会导致该组件显示空白
import 'vant/lib/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// import {useDemoSetupStore} from "@/stores/demoSetupStore.js";
import component1 from "@/utils/EventBus2.js";
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

console.log("vite环境变量：是否处于开发环境", import.meta.env.DEV)
//
const app = createApp(App)
// 全局注册组件
// 导入@element-plus/icons-vue所有图标并进行全局注册
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

//字节跳动图标库
import {install} from '@icon-park/vue-next/es/all';
// import '@icon-park/vue-next/styles/index.css';
install(app);

// 注册全局指令
app.directive('loading', loading);
app.directive('focus', focus);
// 使用插件
// createRouter()
// createStore
app.use(router)
app.use(store)
app.use(Toast)
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
app.use(pinia)
//
app.mount('#app')



