import { ref, computed } from 'vue'

import { defineStore } from 'pinia'
import {getData} from "@/utils/api/shop.js";
export const useDemoSetupStore = defineStore('demoSetupStore', () => {
    const count = ref(12)
    const doubleCount = computed(() => count.value * 2)

    const clickBtnMenu=()=> {

    }
    function increment() {
        count.value++
    }
     async function get() {
         await getData()
     }
    const subCount=()=>count.value--

    return { count, doubleCount, increment,subCount }
},{
    // pinia-plugin-persistedstate
    // persist:true
    persist: {
        paths: [],
        debug:true,
        beforeRestore: (ctx) => {
            console.log(`即将恢复 '${ctx.store.$id}'`)
        },
        afterRestore: (ctx) => {
            console.log(`刚刚恢复完 '${ctx.store.$id}'`)
        },
    }
})
