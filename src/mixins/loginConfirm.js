
//用于混入，实现组件属性的复用
//对于方法，对比普通的方法封装，混入可以使用this等组件内部的属性
//会添加到组件对应选项中，对于生命周期钩子，是与组件中的放在数组管理，统一执行
export default {
    //可按照options api编写
    data(){
        return {
            title:'混入标题'
        }
    },
    methods:{
        sayHi(){
            console.log('hi')
        },
        loginConfirmFn(){
            this.$route.push('/login')
        }
    }
//     computed， 生命周期钩子
}
